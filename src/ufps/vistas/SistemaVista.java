/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ufps.vistas;

import Negocio.SistemaVotacion;

/**
 *
 * @author Alessandro Daniele
 */
public class SistemaVista {
    public static void main(String[] args) {
        SistemaVotacion s = new SistemaVotacion();
        String personas = "https://gitlab.com/madarme/archivos-persistencia/raw/master/votaciones/personas.csv";
        s.cargarPersonas(personas);
        
        
        String departamentos = "https://gitlab.com/madarme/archivos-persistencia/raw/master/votaciones/departamento.csv";
        s.cargarDepartamentos(departamentos);
        
        
        String municipios = "https://gitlab.com/madarme/archivos-persistencia/raw/master/votaciones/municipios.csv";
        s.cargarMunicipios(municipios);
        
        
        String centros= "https://gitlab.com/madarme/archivos-persistencia/raw/master/votaciones/centros.csv";
        s.cargarCentrosDeMesa(centros);
        //System.out.println(s.listadoCentros());
        
        //System.out.println(s.listadoPersonas());
        //System.out.println(s.listadoDepartamentos());
        //System.out.println(s.listadoMunicipios());
        System.out.println(s.listadoNotificaciones());
    }
}
