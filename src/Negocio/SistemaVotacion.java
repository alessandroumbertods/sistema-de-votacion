package Negocio;

import Entidad.*;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.Iterator;
import java.util.Random;
import ufps.util.colecciones_seed.*;
import ufps.util.varios.ArchivoLeerURL;

public class SistemaVotacion {

    private ListaCD<Departamento> dptos = new ListaCD();
    private ListaCD<Persona> personas = new ListaCD();
    private Cola<Notificacion> notificaciones = new Cola();

    public SistemaVotacion() {
        this.personas = new ListaCD();
        this.dptos = new ListaCD();
    }

    public void cargarPersonas(String url) {
        ArchivoLeerURL file = new ArchivoLeerURL(url);
        Object v[] = file.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            String datos = v[i].toString();
            //cedula;nombre;fechanacimiento;id_municipio_inscripcion;email
            String datos2[] = datos.split(";");
            long cedula = Integer.parseInt(datos2[0]);
            String nombre = datos2[1];
            LocalDateTime fecha = getFecha(datos2[2]);
            short id_municipio = Short.parseShort(datos2[3]);
            String email = datos2[4];
            Persona p = new Persona(cedula, nombre, fecha, id_municipio, email);
            if (LocalDateTime.now().getYear() - fecha.getYear() < 60) {
                int random = new Random().nextInt(2);
                if (random % 2 == 0) {
                    p.setEsJurado(true);
                } else {
                    p.setEsSufragante(true);
                }
                personas.insertarAlFinal(p);
            }
        }
    }

    public void cargarDepartamentos(String url) {
        ArchivoLeerURL file = new ArchivoLeerURL(url);
        Object v[] = file.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            String datos = v[i].toString();
            //id_dpto;Nombre Departamento
            String datos2[] = datos.split(";");
            int id = Integer.parseInt(datos2[0]);
            String nombreDpto = datos2[1];
            Departamento dpto = new Departamento(id, nombreDpto);
            this.dptos.insertarAlFinal(dpto);
        }
    }

    public void cargarMunicipios(String url) {
        ArchivoLeerURL file = new ArchivoLeerURL(url);
        Object v[] = file.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            String datos = v[i].toString();
            //id_dpto;id_municipio;nombreMunicipio
            String datos2[] = datos.split(";");
            int idDpto = Integer.parseInt(datos2[0]);
            int idMunicipio = Integer.parseInt(datos2[1]);
            String nombreMunicipio = datos2[2];
            Municipio municipio = new Municipio(idMunicipio, nombreMunicipio);
            for (int j = 0; j < this.dptos.getTamanio(); j++) {
                if (this.dptos.get(j).getId_dpto() == idDpto) {
                    this.dptos.get(j).getMunicipios().insertarAlFinal(municipio);
                }
            }
            //this.dptos.get(idDpto).getMunicipios().insertarAlFinal(municipio); 
        }
    }

    public void cargarCentrosDeMesa(String url) {
        ArchivoLeerURL file = new ArchivoLeerURL(url);
        Object v[] = file.leerArchivo();
        ListaCD<Persona> respaldo = new ListaCD();
        for (int i = 1; i < v.length; i++) {
            String datos = v[i].toString();
            //id_centro;nombreCentro;direccion;id_municipio;cantMesas;maximoSufragantes
            String datos2[] = datos.split(";");
            int id_centro = Integer.parseInt(datos2[0]);
            String nombreCentro = datos2[1];
            String direccion = datos2[2];
            int id_municipio = Integer.parseInt(datos2[3]);
            int cantMesas = Integer.parseInt(datos2[4]);
            int maximoSufragante = Integer.parseInt(datos2[5]);

            CentroVotacion centro = new CentroVotacion(id_centro, nombreCentro, direccion, maximoSufragante);
            crearMesa(cantMesas, maximoSufragante, id_municipio, centro, respaldo);

            boolean insertado = false;
            for (int j = 0; j < this.dptos.getTamanio() && !insertado; j++) {
                for (int k = 0; k < this.dptos.get(j).getMunicipios().getTamanio() && !insertado; k++) {
                    if (this.dptos.get(j).getMunicipios().get(k).getId_municipio() == id_municipio) {
                        this.dptos.get(j).getMunicipios().get(k).getCentros().insertarAlFinal(centro);
                        insertado = true;
                    }
                }
            }
        }
        for (Persona p : this.personas) {
            Notificacion n = new Notificacion(p);
            this.notificaciones.enColar(n);
        }
        restaurarPersonas(respaldo);
    }

    private LocalDateTime getFecha(String fecha) {
        String fecha2[] = fecha.split("-");
        return LocalDateTime.of(Integer.parseInt(fecha2[0]), Integer.parseInt(fecha2[1]), Integer.parseInt(fecha2[2]), 0, 0);
    }

    public ListaCD<Departamento> getDptos() {
        return dptos;
    }

    public void setDptos(ListaCD<Departamento> dptos) {
        this.dptos = dptos;
    }

    public ListaCD<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(ListaCD<Persona> personas) {
        this.personas = personas;
    }

    public Cola<Notificacion> getNotificaciones() {
        return notificaciones;
    }

    public void setNotificaciones(Cola<Notificacion> notificaciones) {
        this.notificaciones = notificaciones;
    }

    public String listadoPersonas() {
        String msg = "";
        for (Persona p : this.personas) {
            msg += p.toString() + "\n";
        }
        return msg;
    }

    public String listadoDepartamentos() {
        String msg = "";
        for (Departamento p : this.dptos) {
            msg += p.toString() + "\n";
        }
        return msg;
    }

    public String listadoMunicipios() {
        String msg = "";
        for (int i = 0; i < this.dptos.getTamanio(); i++) {
            for (Municipio m : this.dptos.get(i).getMunicipios()) {
                msg += m.toString() + "\n";
            }
        }
        return msg;
    }

    public String listadoNotificaciones() {
        String msg = "";
        Cola<Notificacion> respaldo = new Cola();
        while (!this.notificaciones.esVacia()) {
            Notificacion n = this.notificaciones.deColar();
            respaldo.enColar(n);
            msg += n.toString() + "\n";
        }
        this.notificaciones = respaldo;
        return msg;
    }

    public void crearMesa(int cantMesas, int cantidadSufragante, int id_municipio, CentroVotacion centro, ListaCD<Persona> respaldo) {
        String nombreMunicipio = nombre(id_municipio, true);
        String nombreDpto = nombre(id_municipio, false);
        for (int j = 0; j < cantMesas; j++) {
            int sufragantesInsertados = 0;
            Persona jurado[] = new Persona[3];
            Cola<Persona> colaSufragantes = new Cola();
            Integer indice = 0;
            for (int k = 0; k < this.personas.getTamanio(); k++) {
                if (indice == 3 && sufragantesInsertados == cantidadSufragante) {
                    break;
                }
                Persona p = this.personas.get(k);
                crearNotificacion(p, id_municipio, nombreMunicipio, nombreDpto, indice, jurado, sufragantesInsertados, cantidadSufragante, colaSufragantes, respaldo, k);
            }
            if (colaSufragantes.esVacia() || indice == 3) {
                Notificacion notificacion = new Notificacion(nombreDpto, nombreMunicipio, null, null);
                notificaciones.enColar(notificacion);
            }
            crearMesa(j + 1, colaSufragantes, jurado, centro);
        }
    }

    public void crearNotificacion(Persona p, int id_municipio, String nombreMunicipio, String nombreDpto, Integer indice, Persona jurado[], int sufragantesInsertados, int cantidadSufragante, Cola<Persona> colaSufragantes, ListaCD<Persona> respaldo, int k) {
        if (p.getId_Municipio_inscripcion() == id_municipio) {
            Notificacion notificacion = new Notificacion(nombreDpto, nombreMunicipio, LocalDateTime.now(), p);
            if (p.isEsJurado()) {
                if (indice < 3) {
                    jurado[indice] = p;
                    indice++;
                    notificaciones.enColar(notificacion);
                }
            } else {
                if (cantidadSufragante != sufragantesInsertados) {
                    colaSufragantes.enColar(p);
                    notificaciones.enColar(notificacion);
                    sufragantesInsertados++;
                }
            }
            respaldo.insertarAlFinal(p);
            this.personas.eliminar(k);
        }
    }

    public String nombre(int id_municipio, boolean municipio) {
        String msg = "";
        boolean encontrado = false;
        for (int j = 0; j < this.dptos.getTamanio() && !encontrado; j++) {
            for (int k = 0; k < this.dptos.get(j).getMunicipios().getTamanio() && !encontrado; k++) {
                if (this.dptos.get(j).getMunicipios().get(k).getId_municipio() == id_municipio) {
                    if (municipio) {
                        return this.dptos.get(j).getMunicipios().get(k).getNombre();
                    } else {
                        return this.dptos.get(j).getNombreDpto();
                    }
                }
            }
        }
        return "No existe";
    }

    public void restaurarPersonas(ListaCD<Persona> respaldo) {
        for (Persona p : respaldo) {
            this.personas.insertarAlFinal(p);
        }
    }

    public void crearMesa(int numeroDeMesa, Cola<Persona> colaSufragantes, Persona jurado[], CentroVotacion centro) {
        Mesa m = new Mesa(numeroDeMesa, colaSufragantes, jurado);
        centro.getMesas().apilar(m);
    }
}
