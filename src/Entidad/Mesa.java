package Entidad;

import java.util.Arrays;
import ufps.util.colecciones_seed.Cola;

public class Mesa {

    private int id_mesa;
    private Cola<Persona> sufragantes=new Cola();
    private Persona[] jurados=new Persona[3];

    public Mesa() {
    }

    public Mesa(int id_mesa, Cola sufragantes, Persona[] jurados) {
        this.id_mesa = id_mesa;
        this.jurados = jurados;
        this.sufragantes = sufragantes;
    }

    public int getId_mesa() {
        return id_mesa;
    }

    public void setId_mesa(int id_mesa) {
        this.id_mesa = id_mesa;
    }

    public Cola<Persona> getSufragantes() {
        return sufragantes;
    }

    public void setSufragantes(Cola<Persona> sufragantes) {
        this.sufragantes = sufragantes;
    }

    public Persona[] getJurados() {
        return jurados;
    }

    public void setJurados(Persona[] jurados) {
        this.jurados = jurados;
    }

    @Override
    public String toString() {
        return "Mesa " + id_mesa + "\n"+"Sufragantes: \n" + sufragantes + "\n"+  "Jurados: \n" + Arrays.toString(jurados) + "\n";
    }
    
    
}
