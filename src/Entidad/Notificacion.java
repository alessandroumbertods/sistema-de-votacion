package Entidad;

import java.time.LocalDateTime;

public class Notificacion {

    private String nombreDpto;
    private String nombreMunicipio;
    private LocalDateTime fechaVotacion;
    private Persona persona;

    public Notificacion() {
    }

    public Notificacion(String nombreDpto, String nombreMunicipio, LocalDateTime fechaVotacion, Persona persona) {
        this.nombreDpto = nombreDpto;
        this.nombreMunicipio = nombreMunicipio;
        this.fechaVotacion = fechaVotacion;
        this.persona = persona;
    }

    public Notificacion(Persona p) {
        this.persona = p;
    }

    public String getNombreDpto() {
        return nombreDpto;
    }

    public void setNombreDpto(String nombreDpto) {
        this.nombreDpto = nombreDpto;
    }

    public String getNombreMunicipio() {
        return nombreMunicipio;
    }

    public void setNombreMunicipio(String nombreMunicipio) {
        this.nombreMunicipio = nombreMunicipio;
    }

    public LocalDateTime getFechaVotacion() {
        return fechaVotacion;
    }

    public void setFechaVotacion(LocalDateTime fechaVotacion) {
        this.fechaVotacion = fechaVotacion;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    @Override
    public String toString() {
        if (this.persona == null) {
            return "Centro de mesa del Departamento " + nombreDpto + " del municipio " + nombreMunicipio + " es anulado por falta de jurados y/o sufragantes";
        } else {
            if (this.nombreDpto == null) {
                return "La persona " + persona.getNombre() + " no tiene centro de mesa";
            }
            String cargo = (persona.isEsJurado()) ? "Jurado" : "Sufragante";
            return "La persona " + persona.getNombre() + " le toca ser " + cargo + " \t en el Departamento " + nombreDpto + " en el Municipio " + nombreMunicipio + " el dia " + fechaVotacion;
        }
    }
}
